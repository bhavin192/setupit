# setupit: Fedora workstation setup

Ansible playbook to setup a Fedora 31+ workstation.

dotfiles are cloned from
[https://gitlab.com/bhavin192/dotfiles](https://gitlab.com/bhavin192/dotfiles).

### Prerequisites
- Install required packages and Ansible.
  ```sh
  sudo dnf install python3-psutil
  pip3 install --user ansible
  ```
- Clone the repository and cd into it.
  ```sh
  git clone https://gitlab.com/bhavin192/setupit.git
  cd setupit
  ```

### How to use
Follow these steps to setup your Fedora workstation:
- Playbook can be run with following command.
  ```sh
  ansible-playbook -K workstation.yaml
  ```

  It will prompt for user_name, user_email, user_signingkey and
  work_email which will be used for Git configuration.

  - You can pass the values using `--extra-vars` as part of command
    itself.
    ```sh
    ansible-playbook -K workstation.yaml \
        --extra-vars "user_name='Bhavin Gandhi' user_email=mail@host.com"
     ```
  - These values along with any other configuration can be passed
    using a YAML file as well.
	```yaml
	# local-config.yaml
	user_name: "Bhavin Gandhi"
    user_email: mail@host.com
	```
	```sh
	ansible-playbook -K workstation.yaml --extra-vars @local-config.yaml
	```

### Configuration for the playbook
Versions for the tools like kubectl, minikube etc can be configured by
changing the values present in `vars/binaries.yaml`.

```yaml
---
k8s_version: v1.16.3
minikube_version: v1.5.2
helm2_version: v2.16.1
helm3_version: v3.0.1
dive_version: 0.9.1
stern_version: 1.11.0
stern_sha256: e0b39dc26f3a0c7596b240…fdc18c7ad28c833c9eb7db
hugo_version: 0.51
…
```

The `vars/config.yaml` file has a few more configurations which decide
wheather to insatll Docker CE, which Chromium based browser to install
etc.

## Licensing
setupit is licensed under GNU General Public License v3.0. See
[LICENSE](./LICENSE) for the full license text.
