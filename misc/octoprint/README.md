# OctoPrint setup for Fedora Workstation

This playbook sets up OctoPrint on the machine in a Python virtual
environment with a dedicated `octoprint` user. It is based on [Setting
up OctoPrint on a computer running Fedora, CentOS, AlmaLinux or
RockyLinux](https://community.octoprint.org/t/setting-up-octoprint-on-a-computer-running-fedora-centos-almalinux-or-rockylinux/37137/).

### Prerequisites

Install Ansible and clone this repository as described in the [main Prerequisites](../../README.md#prerequisites).

Get the required modules by running the following command:

```shell
ansible-galaxy install -r requirements.yaml
```

### How to use
Take a look at [octoprint.yaml](./octoprint.yaml) for required input
variables. Example input values can be found in the
[example-config.yaml](./example-config.yaml) file.

Run the playbook with following command:

```shell
ansible-playbook -K octoprint.yaml --extra-vars @example-config.yaml
```
