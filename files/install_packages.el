;; Install the packages given at
;; https://gitlab.com/bhavin192/dotfiles/#gnu-emacs-emacs from
;; melpa. Magit is installed from melpa-stable.

;; This file is loaded with following command:
;; yes "y" | emacs --batch --load files/install_packages.el

(package-initialize)
(custom-set-variables
 '(package-selected-packages
   (quote
    (bash-completion dockerfile-mode elfeed elpy epresent go-mode groovy-mode ledger-mode magit markdown-mode org-scrum spaceline spacemacs-theme undo-tree yaml-mode))))

(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; Install magit from melpa-stable
(package-refresh-contents)
(package-install 'magit)
;; (package-install-selected-packages)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(package-refresh-contents)
(package-install-selected-packages)
